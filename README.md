# think-afs

#### 介绍
自定义一个Afs服务-[不是阿里云的Afs调用]

### 安装教程

```shell
# 安装
$ composer require stevema/think-afs
```

### 使用说明

```php
# 1、生成配置文件 - composer 是自动执行这一步的 如果没生成config文件可以手动执行
$ php think vendor:publish

# 2、修改配置文件 /config/afs.php 或在 /.env 文件中添加配置
AFS_SECRET=  # 加密用到的串
AFS_EXPIRE=  # 过期时间 默认600(秒)
AFS_SCENES=  # 场景-多个中间加逗号分隔 默认: 'nvc_login,nvc_register,slide_login,slide_register,click_login,click_register'
```

### 目录说明

```
├─ src
│   ├─ config           # 配置文件目录
│   │   └─ afs.php      # 配置文件
│   ├─ facades          # 门面文件目录
│   │   └─ Afs.php      # 门面
│   └─ AfsCrype.php     # 加密解密文件
│   └─ AfsException.php # 异常文件
│   └─ AfsManage.php    # 项目主文件
│   └─ AfsService.php   # TP使用的Service文件
│   └─ helper.php       # 帮助文件
└─ composer.json        # 配置

```

### 使用方式

```php
# 引用门面
use Stevema\Afs\Facades\Afs;

# 随便加一个路由
Route::get('/t/afs', function(){
    # 定义 scene 和 sessionId
    $scene = 'nvc_login';
    $sessionId = 'sessionId';
    
    # 第一种方式 来自门面 Stevema\Afs\Facades\Afs 上面use的
//    $token = Afs::createToken($scene,$sessionId);
//    $check = Afs::checkToken($scene,$sessionId, $token);
//    $message = Afs::getErrorMessage();
    
    # 如果不引入 这样用
//    $token = \Stevema\Afs\Facades\Afs::createToken($scene,$sessionId);
//    $check = \Stevema\Afs\Facades\Afs::checkToken($scene,$sessionId, $token);
//    $message = \Stevema\Afs\Facades\Afs::getErrorMessage();
    
    # 第二种方式 来自 app->bind('afs', Stevema\Afs\AfsManage::class)
//    $afs = app('afs');
//    $token = $afs->createToken($scene,$sessionId);
//    $check = $afs->checkToken($scene,$sessionId, $token);
//    $message = $afs->getErrorMessage();
    
    # 第三种方式  来自 helper 
    $afs = afs();
//    $token = $afs->createToken($scene,$sessionId);
//    $check = $afs->checkToken($scene,$sessionId, $token);
//    $message = $afs->getErrorMessage();
    # 第四种方式  
    $token = afs_create_token($scene, $sessionId);
    $check = afs_check_token($scene, $sessionId, $token);
    $message = afs_error_message();
        
    dd([
        'scene' => $scene,
        'sessionId' => $sessionId,
        'token' => $token,
        'check' => $check,
        'message' => $message
    ]);
});


# 非TP框架
    require __DIR__.'/vendor/autoload.php';
    
    $scene = 'nvc_login';
    $sessionId = 'sessionId';
    
    $conf = [
        # 加密的字符串secret
        'afs_secret' => "stevema",
        # Afs 可以保留的时间 - 过了时间会提示超时 并且无法验证通过
        'afs_expire' => 600,
        # 方式: nvc(无感知) slide(滑动) click(点击) 等 _ 场景
        # 比如 nvc_login_h5  无感知登录H5
        # 默认提供几个 这个是多个场景拼接的  这是字符串  设置env的时候请用逗号分隔
        'afs_scenes' => implode(",", [
            'nvc_login','nvc_register','slide_login','slide_register',
            'click_login','click_register'
        ]),
    ];
    # 第一种方法
//    use Stevema\Afs\AfsManage;
//    $afs = new AfsManage();
//    $afs->setConfig($conf);
//    # 如果config为空 或者想修改
//    $token = $afs->createToken($scene,$sessionId);
//    # 解密
//    $check = $afs->checkToken($scene,$sessionId, $token);
//    $message = $afs->getErrorMessage();
    # 第二种方法 helper.php 就用一个afs()就行了 其他的别用
//    $afs = afs();
//    $afs->setConfig($conf);
//    $token = $afs->createToken($scene,$sessionId);
//    $check = $afs->checkToken($scene,$sessionId, $token);
//    $message = $afs->getErrorMessage();

    var_dump([
        'scene' => $scene,
        'sessionId' => $sessionId,
        'token' => $token,
        'check' => $check,
        'message' => $message
    ]);

```

### 验证规则
```php
# Afs 添加了自己的验证规则
#  afs_scene  检验 scene是否在配置列表中
#  afs_token:sceneKey,sessionIdKey  校验 token 是不是正确的 注意 冒号后面的参数是你传过来的key  
#  示例如下

 protected $rule = [
    'scene' => ['require', 'afs_scene'],
    'sessionId' => ['require'],
    'token' => ['require', 'afs_token:scene,sessionId'],
];
# afs_token 校验的是 传过来的 token 后面的参数是scene,sessionId 是你传过来的key
# 错误了能在 getError()中获取 

try {
    $result = validate()
        ->batch(true)
        ->check(request()->all(),[
            'scene' => ['require', 'afs_scene'],
            'sessionId' => ['require'],
            'token' => ['require', 'afs_token:scene,sessionId'],
        ]);
    if($result !== true){
        dump($result);
    }
} catch (ValidateException $e) {
    // 验证失败 输出错误信息
    dump($e->getError());
}


```


### 备注

