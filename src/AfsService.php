<?php
namespace stevema\afs;

use think\facade\Config;
use think\Route;
use think\Service as BaseService;
use think\Validate;
use stevema\afs\AfsException;

class AfsService extends BaseService
{
    public function register()
    {
        $this->app->bind('afs', AfsManage::class);
    }
    public function boot()
    {
        app('afs')->setConfig(Config::get('afs'));
        Validate::maker(function ($validate) {
            $validate->extend('afs_scene', function ($value, $rule = '', $data = '', $field = '') {
                try{
//                    app('afs')->checkScene($value);
                    afs_check_scene($value);
                    return true;
                } catch (\stevema\afs\AfsException $e){
                    return false;
                }
            },'scenes is not in whitelist');
        });
        Validate::maker(function ($validate) {
            $validate->extend('afs_token', function ($value, $rule = '', $data = '', $field = '') {
                [$sceneKey, $sessionIdKey] = explode(",", $rule);
                $scene = $data[$sceneKey];
                $sessionId = $data[$sessionIdKey];
//                $check = app('afs')->checkToken($scene, $sessionId, $value);
                $check = afs_check_token($scene, $sessionId, $value);
//                if(!$check) return app('afs')->getErrorMessage();
                if(!$check) return afs_error_message();
                return true;
            },afs_error_message());
        });

//        $this->registerRoutes(function (Route $route) {
//            $route->get('captcha/[:config]', "\\think\\captcha\\CaptchaController@index");
//        });
    }
}