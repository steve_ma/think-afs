<?php
namespace stevema\afs;

use stevema\afs\AfsCrypt as Crypt;
use stevema\afs\AfsException;
use Exception;

class AfsManage{
    /**
     * 配置信息
     * @var
     */
    private mixed $config = null;
    /**
     * 解密的错误信息
     * @var string
     */
    public string $error_message = 'ok';

    /**
     * @param array $config
     */
    public function __construct(){
    }

    /**
     * @param array $config
     * @return void
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    /**
     * @param String $scene
     * @return bool
     * @throws \stevema\afs\AfsException
     */
    public function checkScene(String $scene):bool
    {
        if(empty($scene)) {
            throw new AfsException("scene is validation failed");
        }
        # 场景值必须合法
        if(!in_array($scene, explode(",", $this->config['afs_scenes']))){
            throw new AfsException("scenes is not in whitelist");
        }
        return true;
    }

    /**
     * @param String $sessionId
     * @return bool
     * @throws \stevema\afs\AfsException
     */
    public function checkSessionId(String $sessionId):bool
    {
        if(empty($sessionId)) throw new AfsException("sessionId is validation failed");
        return true;
    }

    /**
     * @param String $scene
     * @param String $sessionId
     * @return string
     * @throws \stevema\afs\AfsException
     */
    public function createToken(String $scene, String $sessionId): string
    {
        try{
            $this->checkScene($scene);
            $this->checkSessionId($sessionId);
            $time = time()+$this->config['afs_expire'];
            # 然后开始加密
            return Crypt::encrypt([
                #第一个参数 scene
                $scene,
                # sessionId
                $sessionId,
                # secret
                $this->config['afs_secret'],
                # timestamp - 过期时间
                $time,
                #sign
                md5($scene.$sessionId.$this->config['afs_secret'].$time),
            ], $this->config['afs_secret']);
        }catch (Exception $e){
            throw new AfsException($e->getMessage());
        }
    }

    /**
     * @param String $scene
     * @param String $sessionId
     * @param String $token
     * @return bool
     */
    public function checkToken(String $scene, String $sessionId, String $token): bool
    {
        try{
            $this->checkScene($scene);
            $this->checkSessionId($sessionId);
            # 然后开始解密
            try {
                [
                    #第一个参数 scene
                    $token_scene,
                    # sessionId
                    $token_sessionId,
                    # secret
                    $token_secret,
                    # timestamp - 过期时间
                    $token_timestamp,
                    #sign
                    $token_sign,
                ] = Crypt::decrypt($token, $this->config['afs_secret']);
            } catch (\Exception $e){
                throw new AfsException("token is validation failed 1");
            }
            # 解码后 验证一下
            if(md5($token_scene.$token_sessionId.$token_secret.$token_timestamp) != $token_sign) {
                throw new AfsException("token is validation failed 2");
            }
            # scene sessionId secret 是不是和初始化的一致
            if($token_scene != $scene || $token_sessionId != $sessionId || $token_secret != $this->config['afs_secret']){
                throw new AfsException("token is validation failed 3");
            }
            # 解码后 验证一下时间是不是超过了
            if(time() > intval($token_timestamp)) {
                throw new AfsException("token is already timed out");
            }
            return true;
        }catch (\Exception $e){
            $this->error_message = $e->getMessage();
            return false;
        }
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->error_message;
    }
}
